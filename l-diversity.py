
# coding: utf-8

# # Data Privacy
# ## Trabalho 2
# - Leonardo Sampaio
# - Lucas S. Fernandes
# - Lucas Primo Muraro
# 

# ### Leitura do dataset

# In[127]:


import pandas as pd
from datetime import datetime
from random import randint
from collections import Counter

dataset = pd.read_csv("epidemias.csv")
dataset.columns = ["nome", "genero", "data_nascimento", "cidade", "estado", "doenca"]


# ### Exclusão dos identificadores e formatando data

# In[128]:


def prepararDados(dataset):
    datasetAnon = dataset.copy()
    datasetAnon['ano_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).year
    datasetAnon['mes_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).month
    datasetAnon['dia_nascimento'] = pd.DatetimeIndex(datasetAnon['data_nascimento']).day
    del datasetAnon['nome']
    del datasetAnon['data_nascimento']
    return datasetAnon
datasetAnon = prepararDados(dataset)


# ### Estrutura de dados utilizada
# Árvore, onde as folhas são os dados e cada nível acima é um nível de generalização. Os níveis possíveis, das folhas à raiz, são:
# - dados originais
# - data_nascimento: -/MM/AA
# - data_nascimento: -/-/AA
# - data_nascimento: -/-/-
# - data_nascimento: -/-/- e cidade: -
# - data_nascimento: -/-/-, cidade: - e estado: -
# - data_nascimento: -/-/-, cidade: -, estado: - e gênero: -
# 
# A ideia é classificar os dados, onde cada folha é uma classe e o nível indica que dado foi generalizado (e como).

# ### Primeiro passo
# Para termos a estrutura de dados, devemos ordenar o dataset para cada nível da árvore.

# In[129]:


def converterDataset(dataset):
    datasetAnon = prepararDados(dataset)
    datasetAnon.sort_values(by=['genero','estado','cidade','ano_nascimento','mes_nascimento','dia_nascimento'], inplace = True);
    return datasetAnon
datasetAnon = converterDataset(dataset)

def doencasSemRepeticao(df):   
    doencas = []
    doencas.append(df.iloc[0]['doenca'])
    for i in range (1,len(df)):
        if(df.iloc[i]['doenca'] not in doencas):
            doencas.append(df.iloc[i]['doenca'])
    return doencas

def doencasComRepeticao(dataset):
    df = dataset.reset_index(drop = True).copy()
    doencas = []
    doencas.append(df.iloc[0]['doenca'])
    for i in range (1,len(df)):
        doencas.append(df.iloc[i]['doenca'])
    auxiliar = 0
    
    return doencas

doencas_do_dataset = doencasSemRepeticao(datasetAnon)   


# ### Segundo passo
# Criamos uma coleção de classes, que devem ter tamanho mínimo k. 
# Todo elemento de uma classe tem os semi-indentificadores iguais. 

# In[130]:


def separarClasses(dataset, k):
    datasetAnon = converterDataset(dataset)
    r = k
    r_old = 0
    r_new = r

    total_rows = len(datasetAnon)

    classes = []
    while (r_new <= total_rows):
        x = datasetAnon[r_old:r_new].copy()
        classes.append(x)
        aux = r_old
        r_old = r_new 
        r_new = r_new + r

        if(r_new > total_rows):
            x = datasetAnon[aux:].copy()
            classes[len(classes)-1] = x
    return classes


# ### Terceiro passo
# Funções para verificar os dados dentro de uma classe

# In[131]:


def checkEqual(df, size):
    teste = []
    for i in range(0,size):       
        for j in range(0,size):
            if(i != j):
                if(df.iloc[i]['dia_nascimento'] != df.iloc[j]['dia_nascimento']):
                    teste = [False, 'dia_nascimento']
                    return teste
                else:
                    if(df.iloc[i]['mes_nascimento'] != df.iloc[j]['mes_nascimento']):
                        teste = [False, 'mes_nascimento']
                        return teste
                    else:
                        if(df.iloc[i]['ano_nascimento'] != df.iloc[j]['ano_nascimento']):
                            teste = [False, 'ano_nascimento']
                            return teste
                        else:
                                if(df.iloc[i]['cidade'] != df.iloc[j]['cidade']):
                                    teste = [False, 'cidade']
                                    return teste
                                else:
                                    if(df.iloc[i]['estado'] != df.iloc[j]['estado']):
                                        teste = [False, 'estado']
                                        return teste
                                    else:
                                        if(df.iloc[i]['genero'] != df.iloc[j]['genero']):
                                            teste = [False, 'genero']
                                            return teste
                                        else:
                                            teste = [True]
    return teste


def posicaoElementoMaisFrequente(lista_de_doencas):     
    for i in range(0,len(lista_de_doencas)-1):
        contador = 0
        auxiliar = 0
        for j in range (i + 1 , len(lista_de_doencas)):
            if (lista_de_doencas[i]==lista_de_doencas[j]):
                contador = contador + 1
                if (contador > auxiliar):
                    mais_comum = lista_de_doencas[i]
                    auxiliar = contador
    i = 0
    indice = -1
    while( i <len(lista_de_doencas)):
        indice = indice + 1
        if(lista_de_doencas[i] == mais_comum):
            break
        i = i + 1
    return indice
            
      
def anonimizarDoenca(dataset,l,doencas):
    df = dataset.reset_index(drop = True).copy()
    doencas_de_uma_classe = []
    doencas_de_uma_classe = doencasSemRepeticao(df)
    size = len(doencas_de_uma_classe)
    while(size < l):
        verificador = True
        while (verificador):
            atributo = doencas[randint(0,len(doencas)-1)]    
            if (atributo not in doencas_de_uma_classe):
                doencas_de_uma_classe.append(atributo)
                lista = doencasComRepeticao(df)
                posicao = posicaoElementoMaisFrequente(lista)
                df.at[posicao,'doenca'] = atributo
                size = len(doencas_de_uma_classe)
                verificador = False
    return df

def generalizar(df, attr, size):
    df.loc[:, attr] = '*'
    if(attr == 'dia_nascimento'):
        df.loc[:, attr] = '**'
    if(attr == 'mes_nascimento'):
        df.loc[:, attr] = '**'
        df.loc[:, 'dia_nascimento'] = '**'
    if(attr == 'ano_nascimento'):
        df.loc[:, attr] = '**'
        df.loc[:, 'dia_nascimento'] = '**'
        df.loc[:, 'mes_nascimento'] = '**'


# ### Quarto passo
# Anonimizar o dataset, igualando as diferenças dentro de cada classe.

# In[132]:


def anonimizar(dataset, k, l):
    classes = separarClasses(dataset, k)
    classes_total_rows = len(classes)
    for i in range(0,classes_total_rows):
        classes[i] = anonimizarDoenca(classes[i],l,doencas_do_dataset)
        size = len(classes[i])
        aux = checkEqual(classes[i], size)
        while( not aux[0] ):
            generalizar(classes[i], aux[1], size)
            aux = checkEqual(classes[i], len(classes[i]))
    return classes


# ### Finalização em um dataset anonimizado
# Retornando um dataset com estrutura similar ao primeiro, mantendo suas propriedades.

# In[133]:


def dataset_Anonimizado(dataset, k, l):
    classes = anonimizar(dataset, k, l);
    df = []
    for r in classes:
        for x in r.values.tolist():
            df.append(x)
    headers = ['genero','cidade','estado','doenca','ano_nascimento','mes_nascimento','dia_nascimento']        
    datasetAnon = pd.DataFrame(df, columns=headers)
    datasetAnon['data_nascimento'] = datasetAnon[['mes_nascimento','dia_nascimento', 'ano_nascimento']].apply(lambda x: '{}/{}/{}'.format(x[0],x[1],x[2]), axis=1) 
    del(datasetAnon['ano_nascimento'])
    del(datasetAnon['mes_nascimento'])
    del(datasetAnon['dia_nascimento'])
    datasetAnon = datasetAnon[['genero','data_nascimento','cidade','estado','doenca']].copy()
    return datasetAnon


# ### K-anonimity finalizado
# Dado um dataset e um K, retorna-se o dataset anonimizado.

# In[134]:


def k_anonymity(dataset, k,l):
    return dataset_Anonimizado(dataset, k,l)


# ### Salvar dataset anonimizado em CSV

# In[137]:


doencas_originais = doencasComRepeticao(dataset)

k = 10
l = 2
datasetAnon_2 = k_anonymity(dataset, k, l);
datasetAnon_2.to_csv("epidemias_2_Anon_2.csv")

k = 10
l = 5
datasetAnon_4 = k_anonymity(dataset, k, l);
datasetAnon_4.to_csv("epidemias_2_Anon_4.csv")

k = 20
l = 2
datasetAnon_8 = k_anonymity(dataset, k, l);
datasetAnon_8.to_csv("epidemias_2_Anon_8.csv")

k = 20
l = 5
datasetAnon_16 = k_anonymity(dataset, k, l);
datasetAnon_16.to_csv("epidemias_2_Anon_16.csv")


# ### Precisão

# In[136]:


def precisao(dataset):
    Na = 5
    soma = 0
    D = len(dataset)
    for x in dataset:
        for i in range(0, D):
            h = 0
            HGV = 1
            element = dataset.iloc[i][x] 
            if (x == 'genero'):
                if (element == '*'):
                    h = 1
            elif (x == 'cidade'):
                if (element == '*'):
                    h = 1
            elif (x == 'estado'):
                if (element == '*'):
                    h = 1
            elif (x == 'data_nascimento'):
                HGV = 3
                if(element == '**/**/**'):
                    h = 3
                elif ('**/**' in element):
                    h = 2
                elif ('**/' in element):
                    h = 1
            elif (x == 'doenca'):
                if(element != doencas_originais[i]):
                    h = 1
            soma = soma + (h/HGV)
    return 1 - soma/(D*Na)

print("A precisão para k = 10 e l = 2 é de {}".format(precisao(datasetAnon_2)))
print("A precisão para k = 10 e l = 5 é de {}".format(precisao(datasetAnon_4)))
print("A precisão para k = 20 e l = 2 é de {}".format(precisao(datasetAnon_8)))
print("A precisão para k = 20 e l = 5 é de {}".format(precisao(datasetAnon_16)))

